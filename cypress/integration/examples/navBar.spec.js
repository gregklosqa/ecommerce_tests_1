beforeEach(() => {
    cy.viewport('macbook-13')
})

describe('NavBar Tests', () => {
    before(function() {
        cy.visit('https://www.clarkesfurnishers.co.uk')
        cy.get('#main-menu')
    
    })
    
    it('should display Living Room content', () => {
        
        cy.contains('Living Room').click()
        cy.url().should('include', 'living-room')
        cy.get('h1').should('be.visible')
        cy.get('#breadcrumbs').should('contain', 'Living Room')
        cy.request('https://www.clarkesfurnishers.co.uk/living-room').then((response) => {
            expect(response.status).to.eq(200)
        })
    })
    
    it('should display Bedroom content', () => {

        cy.contains('Bedroom').click()
        cy.url().should('include', 'bedroom')
        cy.get('h1').should('contain', 'Bedroom')
        cy.get('#breadcrumbs').should('contain', 'Bedroom')

    })

    it('should display Dining Room content', () => {

        cy.contains('Dining Room').click()
        cy.url().should('include', 'dining-room')
        cy.get('h1').should('contain', 'Dining Room')
        cy.get('#breadcrumbs').should('contain', 'Dining Room')

    })
    
    it('should display In Stock content', () => {

        cy.contains('In Stock').click()
        cy.url().should('include', 'category&path=1583')
        cy.get('h1').should('contain', 'In Stock')
        cy.get('#breadcrumbs').should('contain', 'In Stock')

    })

    it('should display Clearance content', () => {

        cy.contains('Clearance').click()
        cy.url().should('include', 'clearance')
        cy.get('h1').should('contain', 'Clearance')
        cy.get('#breadcrumbs').should('contain', 'Clearance')

    })

    it('should display Accessories content', () => {

        cy.contains('Accessories').click()
        cy.url().should('include', 'accessories')
        cy.get('h1').should('contain', 'Accessories')
        cy.get('#breadcrumbs').should('contain', 'Accessories')

    })

    it('should display Brands content', () => {
        cy.get('[href="brands"]').click()
        cy.url().should('include', 'brands')
        
    })

    it('nav bar should contain 7 elements', () => {

        const array = cy.get('#main-menu > ul > li > a')
        array.should('have.length',7).wait(5000)
        
    })
})
