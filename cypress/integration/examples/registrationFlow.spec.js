import register from "../../page-model/registration"

beforeEach(() => {
    cy.viewport('macbook-13')
})

describe('Register a new account',() => {
  
    it('should visit register page', () => {
        cy.visit('https://www.clarkesfurnishers.co.uk')
        cy.get('[class="fa fa-user icon"]').click()
        cy.get('[class="breadcrumb clearfix"]')//.should('contain', 'login')
        cy.get('.well > .std-btn').click()
        cy.url().should('contain', 'register')
    })

    it('should fill person details fields', () => {
    
        register.getElemenetAndType(register.selector.name, register.name)
        register.getElemenetAndType(register.selector.lastName, register.lastName)
        register.getElemenetAndType(register.selector.email, register.email)
        register.getElemenetAndType(register.selector.telephone, register.telephone)
        register.getElemenetAndSelect(register.selector.contactMethod, register.contactMethod)
    })

    it('should fill adress fields', () => {
        
        register.getElemenetAndType(register.selector.postCode, register.postCode)
        register.getElemenetAndClick(register.selector.postCodeList)
        register.getElemenetAndType(register.selector.adressLine2, register.adressLine2)
        register.getElemenetAndType(register.selector.city, register.city)
        register.getElemenetAndScrollIntoView(register.selector.country)//.click()
        register.getElemenetAndScrollIntoView(register.selector.county)
        //.click({ force: true })
    })

    it('should  create password', () => {
        register.getElemenetAndType(register.selector.password, register.password)
        register.getElemenetAndType(register.selector.confirmPassword, register.confirmPassword)
    })

    it('should click Newsletter checbox as a No', () => {
        register.getElemenetAndClick(register.selector.checkbox)
        cy.get('.radio-inline [value="0"]').should('exist')
    })   

    it('should agree with privacy policy', () => {
        register.getElemenetAndClick(register.selector.privacyPolicy)
        register.getElement(register.selector.continueButton)

    })
})
