beforeEach(() => {
    cy.viewport('macbook-13')
})

describe('Search Tests', () => {
    before(function(){
        cy.viewport('macbook-13')
    })
        
    it('search form should type sofa', () => {

    cy.visit('https://www.clarkesfurnishers.co.uk')
    cy.get('.search-form').type('sofa {enter}')
    cy.get('h1').should('contain', 'Search - sofa')
    cy.url().should('include', 'search=sofa')
        
    })
})
