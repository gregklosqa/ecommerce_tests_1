

class Register {
    constructor () {
        this.name = 'John',
        this.lastName = "Doe",
        this.email = 'john.doe@wp.com',
        this.telephone = '123123123'
        this.contactMethod = 'Email',
        this.postCode = 'BD9 4NP',
        this.adressLine2 = 'Selborne Mount',
        this.city = 'Bradford',
        this.password = 'password',
        this.confirmPassword = 'password',
        this.selector = {
            name: '#input-firstname',
            lastName: '#input-lastname',
            email: '#input-email',
            telephone: '#input-telephone',
            contactMethod: '#input-custom-field3',
            postCode: '#input-postcode',
            postCodeList: '.pca [class="pcaitem pcafirstitem pcaselected"]',
            adressLine2: '#input-address-2',
            city: '#input-city',
            country: '#input-country [value="222"]',
            county: '#input-zone [value="3608"]',
            password: '#input-password',
            confirmPassword: '#input-confirm',
            checkbox: 'input[type="checkbox"]',
            privacyPolicy: '.checkbox [value="1"]',
            continueButton: '.buttons [value="Continue"]'
        } 
    }
    getElemenetAndType = (selector, tekst) => {
        cy.get(selector).type(tekst)
    }
    getElemenetAndSelect = (selector, tekst) => {
        cy.get(selector).select(tekst)
    }
    getElemenetAndClick = (selector) => {
        cy.get(selector).click()
    }
    getElemenetAndScrollIntoView = (selector) => {
        cy.get(selector).scrollIntoView().click({ force: true })
    }
    getElement = (selector) => {
        cy.get(selector)
    }
  }
  export default new Register()